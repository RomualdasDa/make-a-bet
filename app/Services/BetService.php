<?php

declare(strict_types = 1);

namespace App\Services;

use App\Models\BalanceTransaction;
use App\Models\Bet;
use App\Models\BetSelection;
use App\Models\Player;
use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * Class BetService
 * @package App\Services
 */
class BetService
{
    /**
     * @param array $data
     * @return float
     */
    public function process(array $data): float
    {
        $selections = Collection::make($data['selections']);

        // @TODO use DB transactions
        // @TODO use repository for DB stuff maybe

        $player = Player::firstOrCreate(
            ['id' => $data['player_id']],
            ['balance' => 1000]
        );

        $betResult = $selections->sum(fn(array $selection) => $selection['odds'] * $data['stake_amount']);
        $realWinning = $betResult - $data['stake_amount'];

        // set bet amount
        $bet = Bet::create([
            'player_id' => $player->id,
            'stake_amount' => $data['stake_amount'],
        ]);

        // set transaction data
        BalanceTransaction::create([
            'player_id' => $player->id,
            'amount' => $player->balance + $realWinning,
            'amount_before' => $player->balance,
        ]);

        // prepare bet selection data
        $betSelectionData = [];

        $selections->each(function(array $selection) use (&$betSelectionData, $bet) {
            $betSelectionData[] = [
                'bet_id' => $bet->id,
                'selection_id' => $selection['id'],
                'odds' => $selection['odds'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        });

        // insert bet selection data in single go
        BetSelection::insert($betSelectionData);

        // Increase player amount based on winning (reduced bet amount)
        $player->increment('balance', $realWinning);

        return $selections->map(fn(array $selection) => $selection['odds'] * $data['stake_amount'])->first();
    }
}

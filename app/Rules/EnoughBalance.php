<?php

namespace App\Rules;

use App\Models\Player;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Collection;

/**
 * Class EnoughBalance
 * @package App\Rules
 */
class EnoughBalance implements Rule
{
    /**
     * @var Player
     */
    private Player $player;

    /**
     * @var array
     */
    private array $selections;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(int $playerId, array $selections)
    {
        $this->player = Player::findOrFail($playerId);
        $this->selections = $selections;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $selections = Collection::make($this->selections);

        $requiredBalanceToBet = $selections->sum(fn(array $selection) => $selection['odds'] * $value);

        if ($this->player->balance < $requiredBalanceToBet) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Insufficient balance';
    }
}

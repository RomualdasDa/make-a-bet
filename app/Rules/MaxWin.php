<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Collection;

/**
 * Class MaxWin
 * @package App\Rules
 */
class MaxWin implements Rule
{
    /**
     * @var array
     */
    private array $selections;

    /**
     * @var int
     */
    private int $maxWin;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $selections, int $maxWin)
    {
        $this->selections = $selections;
        $this->maxWin = $maxWin;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $selections = Collection::make($this->selections);

        $betResult = $selections->sum(fn(array $selection) => $selection['odds'] * $value);

        if ($betResult > $this->maxWin) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return "Maximum win amount is {$this->maxWin}";
    }
}

<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class UniqueSelections
 * @package App\Rules
 */
class UniqueSelections implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $prevKey = false;

        foreach($value as $item) {
            if ($prevKey === $item['id']) {
                return false;
            }

            $prevKey = $item['id'];
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Duplicate selection found.';
    }
}

<?php

declare(strict_types = 1);

namespace App\Http\Requests;

use App\Rules\EnoughBalance;
use App\Rules\MaxWin;
use App\Rules\UniqueSelections;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

/**
 * Class BetRequest
 * @package App\Http\Requests
 */
class BetRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'player_id' => ['required', 'int'],
            'selections.*.odds' => ['numeric', 'between:1,10000'],
            'selections' => ['bail','array', 'min:1', 'max:20', new UniqueSelections()],
            'stake_amount' => [
                'bail',
                'required',
                'numeric',
                'min:0.3',
                'max:10000',
                new EnoughBalance($this->input('player_id'), $this->input('selections')),
                new MaxWin($this->input('selections'), 20000),
            ],
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }

    /**
     * @param Validator $validator
     * @return array[]
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator): array
    {
        $data = $validator->getMessageBag();

        // @TODO: transform error to match requirements, maybe use Transformer class

        $responseData = new JsonResponse($data);

        throw new ValidationException($validator, $responseData);
    }
}

<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\BetRequest;
use App\Services\BetService;
use Illuminate\Http\JsonResponse;

/**
 * Class BetController
 * @package App\Http\Controllers
 */
class BetController extends Controller
{
    /**
     * BetController constructor.
     * @param BetService $betService
     */
    public function __construct(protected BetService $betService)
    {
    }

    /**
     * @param BetRequest $request
     * @return JsonResponse
     */
    public function postBet(BetRequest $request): JsonResponse
    {
        $this->betService->process($request->validated());

        return response()->json([], 201);
    }
}
